#! /usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       sway-screenmanager
#       
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
""" 
    This script allow you to define and apply various screen configurations
"""

__author__ = "Florence Birée"
__version__ = "0.2"
__license__ = "GPLv3+"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import subprocess
import json
import argparse
import configparser
from copy import deepcopy
import re

class SwayException(Exception):
    """Exeption raised when swaymsg return an error"""
    def __init__(self, message):
        self.message = message

class SwayOutput:
    """A sway output configuration tool"""
    
    def __init__(self):
        """Initialize the SwayOutput by loading available outputs from sway
        
            Also load available workspaces
        """
        swaymsgt = subprocess.run(['swaymsg', '-t', 'get_outputs'],
                                  capture_output=True)
        self.output_data = json.loads(swaymsgt.stdout)
        self.screens = {}
        for output in self.output_data:
            self.screens[output['name']] = output
        
        swaymsgt = subprocess.run(['swaymsg', '-t', 'get_workspaces'],
                                  capture_output=True)
        self.output_data = json.loads(swaymsgt.stdout)
        self.workspaces = {}
        for workspace in self.output_data:
            self.workspaces[workspace['name']] = workspace
            
        self.debug = False
        
    def dump(self):
        """Dump all sway data for outputs"""
        return json.dumps(self.output_data, sort_keys=True, indent=4)
    
    def _swaymsg(self, screen_name, command):
        """Execute a swaymsg output command, and check the result"""
        cmd = 'output ' +  screen_name + ' ' + command
        self._raw_swaymsg(cmd)
    
    def _raw_swaymsg(self, command):
        """Execute any swaymsg command, and check the result"""
        cmd = ['swaymsg'] + command.split()
        if self.debug:
            print(' '.join(cmd), file=sys.stderr)
        swaymsg = subprocess.run(cmd, capture_output=True) 
        out = json.loads(swaymsg.stdout)
        if not out[0]['success']:
            raise SwayException(out[0]['error']) 

    def enable(self, screen_name):
        """Enable screen `screen_name`"""
        self._swaymsg(screen_name, 'enable')
        
    def disable(self, screen_name):
        """Disable screen `screen_name`"""
        self._swaymsg(screen_name, 'disable')

    def set_mode(self, screen_name, mode):
        """Set the `mode` for `screen_name`"""
        self._swaymsg(screen_name, 'mode %s' % mode)
    
    def set_pos(self, screen_name, x, y):
        """Set the position `x`, `y` for `screen_name`"""
        self._swaymsg(screen_name, 'pos %d %d' % (x, y))
    
    def rotate(self, screen_name, rotation):
        """Set the `rotation` for `screen_name`. Rotation can be `normal`,
            `90`, `180` or `270`.
        """
        self._swaymsg(screen_name, 'transform %s' % rotation)
    
    def scale(self, screen_name, factor):
        """Set the scale `factor` for `screen_name`."""
        self._swaymsg(screen_name, 'scale %s' % factor)
    
    def scale_filter(self, screen_name, sfilter):
        """Set the scale `sfilter` for `screen_name`. `sfilter` can be
            one of 'linear', 'nearest', 'smart'.
        """
        self._swaymsg(screen_name, 'scale_filter %s' % sfilter)
    
    def subpixel(self, screen_name, s_hinting):
        """Set the subpixel hinting. Can be one of 'rgb', 'bgr', 'vrgb',
            'vbgr', 'none'
        """
        self._swaymsg(screen_name, 'subpixel %s' % s_hinting)
    
    def focus(self, screen_name):
        """Move the focus to `screen_name`"""
        self._raw_swaymsg('focus output %s' % screen_name)
    
    def move_workspace(self, screen_name, workspace_name):
        """Move `workspace_name` to `screen_name`"""
        first_focused_workspace = None
        for ws in self.workspaces:
            if self.workspaces[ws]['focused']:
                first_focused_workspace = ws
        
        self._raw_swaymsg(
            'workspace_auto_back_and_forth no ;'
            'workspace {} ,'
            'move workspace to output {} ;'
            'workspace {} ;'
            'workspace_auto_back_and_forth yes'.format(
                workspace_name, screen_name, first_focused_workspace
            )
        )
    
class Screen:
    """A screen object"""
    def __init__(self, from_sway=None, from_config=None):
        self.screen_id = None
        self.name = None
        self.vendor_name = None
        self.default_mode = None
        self.current_mode = None
        self.enabled = None
        self.position = None
        self.primary = None
        self.origin = None
        self.default_transform = None
        self.current_transform = None
        self.scale = None
        self.scale_filter = None
        self.subpixel = None
        self.use_close_status = None
        self.closed = False
        
        if from_sway:
            self.name = [list(from_sway.keys())[0]]
            screen_infos = from_sway[self.name[0]]
            self.vendor_name = ('{} {} {}'.format(
                screen_infos['make'],
                screen_infos['model'],
                screen_infos['serial']
            ))
            def_mode = screen_infos['modes'][-1]
            self.default_mode = {
                'width': def_mode['width'],
                'height': def_mode['height']
            }
            self.enabled = screen_infos['active']
            self.default_transform = 'normal'
            if self.enabled:
                self.current_mode = {
                    'width': screen_infos['current_mode']['width'],
                    'height': screen_infos['current_mode']['height'],
                }
                self.position = {
                    'x': screen_infos['rect']['x'],
                    'y': screen_infos['rect']['y'],
                }
                self.primary = screen_infos['primary']
                self.current_transform = screen_infos['transform']
                self.scale = screen_infos['scale']
                self.scale_filter = screen_infos['scale_filter']
                if screen_infos['subpixel_hinting'] != 'unknown':
                    self.subpixel_hinting = screen_infos['subpixel_hinting']
            self.origin = 'sway'
        
        if from_config:
            self.screen_id = list(from_config.keys())[0]
            screen_infos = from_config[self.screen_id]
            if 'name' in screen_infos:
                self.name = [n.strip() for n in screen_infos['name'].split(',')]
            if 'vendor_name' in screen_infos:
                self.vendor_name = screen_infos['vendor_name']
            if 'default_mode' in screen_infos:
                w, h = (int(x) for x in screen_infos['default_mode'].split('x'))
                self.default_mode = {'width': w, 'height': h}
            if 'default_transform' in screen_infos:
                self.default_transform = screen_infos['default_transform']
            if 'scale' in screen_infos:
                self.scale = screen_infos['scale']
            if 'scale_filter' in screen_infos:
                self.scale_filter = screen_infos['scale_filter']
            if 'subpixel' in screen_infos:
                self.subpixel = screen_infos['subpixel']
            if 'use_close_status' in screen_infos:
                self.use_close_status = screen_infos['use_close_status']
                # retreive the close status
                with open(self.use_close_status) as lid_status:
                    self.closed = ('closed' in lid_status.read())
            self.origin = 'config'
    
    def __eq__(self, oth_scr):
        """Return if this screen is the same than oth_scr"""
        # first compare vendor_name, then name, then screen_id
        if self.vendor_name and oth_scr.vendor_name:
            return self.vendor_name == oth_scr.vendor_name
        if self.name and oth_scr.name:
            return (set(self.name) & set(oth_scr.name))
        return self.screen_id == oth_scr.screen_id
    
    def update(self, oth_scr):
        """Update this screen using data from an actual screen"""
        # get a name
        if not self.name:
            self.name = oth_scr.name
        # if multiple names
        if len(self.name) > 1:
            # get the first common name from oth_scr
            for name in oth_scr.name:
                if name in self.name:
                    self.name = [name]
                    break
        # get a default mode
        if not self.default_mode:
            self.default_mode = oth_scr.default_mode
        # get a default transform
        if not self.default_transform:
            self.default_transform = oth_scr.default_transform
        # get a close status
        if not self.use_close_status:
            self.use_close_status = oth_scr.use_close_status
            self.closed = oth_scr.closed
    
    def __repr__(self):
        """Output the screen representation"""
        if self.current_mode:
            mode = '{width:d}x{height:d}C '.format(**self.current_mode)
        elif self.default_mode:
            mode = '{width:d}x{height:d}D '.format(**self.default_mode)
        else:
            mode = ''
        if self.current_transform:
            transform = self.current_transform + 'C '
        elif self.default_transform:
            transform = self.default_transform + 'D '
        else:
            transform = ''
        if self.use_close_status:
            close_status = "C:{}".format(self.closed)
        else:
            close_status = ''
        return '<{id}{name} {vendor_name}{mode}{pos}{transform}{enabled}{close_status}>'.format(**{
            'id': self.screen_id + ' ' if self.screen_id else '',
            'name': self.name,
            'vendor_name': "'{}' ".format(self.vendor_name) if self.vendor_name else '',
            'mode': mode,
            'pos': '{x:d}x{y:d} '.format(**self.position) if self.position else '',
            'transform': transform,
            'enabled': '*' if self.enabled else '',
            'close_status': close_status,
        })

class State:
    """A screen state, either the current state or a specific screen 
        configuration
    """
    def __init__(self, from_sway=None, from_config=None):
        self.name = None
        self.screens = []
        self.origin = None
        self.workspaces = None
        self.focus = None
        
        if from_sway:
            for screen_name in from_sway:
                self.screens.append(Screen(
                    from_sway={screen_name: from_sway[screen_name]}
                ))
            self.origin = 'sway'
        
        if from_config:
            self.name = from_config['state_id']
            self.origin = 'config'
            config = from_config['config']
            state = config.get_state(self.name)
            # load screens
            screen_id_list = [i.strip() for i in state['enabled'].split(',')]
            for screen_id in screen_id_list:
                self.screens.append(Screen(
                    from_config={screen_id: config.get_screen(screen_id)}
                ))
            # enable all screens (except closed one if only_if_open)
            if 'only_if_open' in state:
                only_open_screens = [i.strip() for i in state['only_if_open'].split(',')]
            else:
                only_open_screens = []
            for screen in self.screens:
                if screen.screen_id in only_open_screens:
                    if not screen.closed:
                        screen.enabled = True
                else:
                    screen.enabled = True
            # apply positions, modes, primary and disposition
            if 'primary' in state:
                for screen in self.screens:
                    if screen.screen_id == state['primary']:
                        screen.primary = True
            if 'positions' in state: # position is prioritary over disposition
                for i, pos in enumerate(i.strip() for i in state['positions'].split(',')):
                    x, y = (int(x) for x in pos.split('x'))
                    self.screens[i].position = {'x': x, 'y': y}
            if 'modes' in state:
                for i, mode in enumerate(i.strip() for i in state['modes'].split(',')):
                    width, height = (int(x) for x in mode.split('x'))
                    self.screens[i].current_mode = {'width': width, 'height': height}
            if 'transform' in state:
                for i, transform in enumerate(i.strip() for i in state['transform'].split(',')):
                    self.screens[i].current_transform = transform
            
            # apply workspace layout
            if 'workspaces' in state:
                self.workspaces = []
                ws_layout = state['workspaces']
                for scr_layout in ws_layout.split(';'):
                    screen_name, workspace_list = scr_layout.split(':')
                    self.workspaces.append((
                            screen_name.strip(),
                            [ws.strip() for ws in workspace_list.split(',')]
                    ))
            # apply focus
            if 'focus' in state:
                self.focus = state['focus']
    
    def __eq__(self, oth_state):
        """Return if self is more or less the same state than oth_state"""
        # check if the enabled screen list is the same
        my_enabled_screens = [s for s in self.screens if s.enabled]
        oth_enabled_screens = [s for s in oth_state.screens if s.enabled]
        if len(my_enabled_screens) != len(oth_enabled_screens):
            return False
        for my_s in my_enabled_screens:
            s_found = False
            for oth_s in oth_enabled_screens:
                if my_s == oth_s:
                    s_found = True
                    # check if current_mode and positions are the sames
                    if my_s.current_mode and oth_s.current_mode and \
                            my_s.current_mode != oth_s.current_mode:
                        return False
                    if my_s.position and oth_s.position and \
                            my_s.position != oth_s.position:
                        return False
                    # check if current_transform is the same
                    if my_s.current_transform and oth_s.current_transform and\
                            my_s.current_transform != oth_s.current_transform:
                        return False
                    break
            if not s_found:
                return False
        # all checks are ok
        return True
    
    def is_possible(self, current_state):
        """Return if this state can be applyed in the current_state"""
        # check if all my enabled screens are in current_state
        my_enabled_screens = [s for s in self.screens if s.enabled]
        for my_s in my_enabled_screens:
            s_found = False
            for cur_s in current_state.screens:
                if my_s == cur_s:
                    s_found = True
                    break
            if not s_found:
                return False
        return True
    
    def update(self, current_state):
        """Complete this state with hardware from current_state"""
        for cur_screen in current_state.screens:
            s_found = False
            for my_s in self.screens:
                if my_s == cur_screen:
                    s_found = True
                    # update my screen with name information
                    my_s.update(cur_screen)
                    break
            if not s_found:
                # add this screen to my state, but disable it
                missing_screen = deepcopy(cur_screen)
                missing_screen.enabled = False
                self.screens.append(missing_screen)
            
    def __repr__(self):
        """State representation"""
        return '<{name}\n    {screens}\n>'.format(**{
            'name': 'CurrentStatus' if self.origin == 'sway' else self.name,
            'screens': '\n    '.join((screen.__repr__() for screen in self.screens)),
        })

class ConsistencyError(Exception):
    """Consistency error in the configuration file"""
    def __init__(self, msg):
        self.msg = msg
    def __repr__(self):
        return self.msg

class ScreenConfig:
    """Manage a screen configuration"""
    
    CONFIG_PATH = [os.path.expanduser('~/.config/swayscreens.ini')]
    
    def __init__(self):
        """Load existing configuration"""
        # find the configuration file
        self.filename = None 
        for path in self.CONFIG_PATH: 
            if os.path.exists(path): 
                self.filename = path
        
        # read the configuration file
        self.data = configparser.ConfigParser(strict=True)
        if self.filename:
            self.data.read(self.filename)
    
    def screens(self):
        """Return the list of defined screens"""
        return [ 
            section.replace('Screen ', '')
            for section in self.data.sections()
            if section.startswith('Screen ')
        ]
    
    def get_screen(self, screen_id):
        """Return the configuration for `screen_id`"""
        return self.data['Screen ' + screen_id]
        
    def states(self):
        """Return the list of screen state configurations"""
        return [ 
            section.replace('State ', '')
            for section in self.data.sections()
            if section.startswith('State ')
        ]
    
    def get_state(self, state_id):
        """Return the configuration for `state_id`"""
        return self.data['State ' + state_id]

    def toggles(self):
        """Return the list of toggles"""
        return [ 
            section.replace('Toggle ', '')
            for section in self.data.sections()
            if section.startswith('Toggle ')
        ]
    
    def get_toggle(self, toggle_id):
        """Return the configuration for `toggle_id`"""
        return self.data['Toggle ' + toggle_id]
    
    def check(self):
        """Perform checkes on the configuration file. Return errors"""
        # utilities
        RE_MODE = re.compile(r'\d+x\d+')
        TRANSFORM_AVAIL_LIST =  ['normal', '90', '180', '270']
        RE_SCALE = re.compile(r'\d+(.\d*)?')
        SCALE_FILTER_LIST = ['linear', 'nearest', 'smart']
        SPH_LIST = ['rgb', 'bgr', 'vrgb', 'vbgr', 'none']
        def _mode_check(mode, context):
            if not RE_MODE.match(mode):
                raise ConsistencyError('The mode `{}` doesn\'t fit the pattern WIDTHxHEIGHT in {}.'.format(
                    mode, context))
        def _transform_check(transform, context):
            if not transform in TRANSFORM_AVAIL_LIST: 
                raise ConsistencyError('The transform `{}` is not one of {} in {}.'.format(
                    transform, TRANSFORM_AVAIL_LIST, context))
        def _position_check(pos, context):
            if not RE_MODE.match(pos):
                raise ConsistencyError('The position `{}` doesn\'t fit the pattern XxY in {}.'.format(
                    pos, context))
        def _scale_check(scale, context):
            if not RE_SCALE.match(scale):
                raise ConsistencyError('The scale must be a number in {}.'.format(
                    context))
        def _scale_filter_check(sfilter, context):
            if not sfilter in SCALE_FILTER_LIST:
                raise ConsistencyError('The scale_filter `{}` is not one of {} in {}.'.format(
                    sfilter, SCALE_FILTER_LIST, context))
        def _subpixel_check(s_hinting, context):
            if not s_hinting in SPH_LIST:
                raise ConsistencyError('The subpixel `{}` is not one of {} in {}.'.format(
                    s_hinting, SPH_LIST, context))
        
        # perform checks on the screens
        for screen_id in self.screens():
            screen = self.get_screen(screen_id)
            context = '[Screen {}]'.format(screen_id)
            # check name / vendor_name
            if not 'name' in screen and not 'vendor_name' in screen:
                raise ConsistencyError('You must define either `name` or `vendor_name` in {}.'.format(
                    context))
            # check default_mode
            if 'default_mode' in screen:
                _mode_check(screen['default_mode'], context)
            # check default_transform
            if 'default_transform' in screen:
                _transform_check(screen['default_transform'], context)
            # check scale
            if 'scale' in screen:
                _scale_check(screen['scale'], context)
            # check scale_filter
            if 'scale_filter' in screen:
                _scale_filter_check(screen['scale_filter'], context)
            # check subpixel
            if 'subpixel' in screen:
                _subpixel_check(screen['subpixel'], context)
        
        # perform checks on the states
        for state_id in self.states():
            state = self.get_state(state_id)
            context = '[State {}]'.format(state_id)
            # check enabled list
            if not 'enabled' in state:
                raise ConsistencyError('You must define enabled screens in {}.'.format(
                    context))
            screen_list = [s.strip() for s in state['enabled'].split(',')]
            for s in screen_list:
                if s not in self.screens():
                    raise ConsistencyError('The screen `{}` referenced in {} is not defined.'.format(
                        s, context))
            # check modes
            if 'modes' in state:
                modes_list = [s.strip() for s in state['modes'].split(',')]
                if len(screen_list) != len(modes_list):
                    raise ConsistencyError('The list of modes in {} doesn\'t have the same number of items than the `enabled` list.'.format(
                        context))
                for mode in modes_list:
                    _mode_check(mode, context)
            # check position
            if 'positions' in state:
                positions_list = [s.strip() for s in state['positions'].split(',')]
                if len(screen_list) != len(positions_list):
                    raise ConsistencyError('The list of positions in {} doesn\'t have the same number of items than the `enabled` list.'.format(
                        context))
                for pos in positions_list:
                    _position_check(pos, context)
            # check transform
            if 'transform' in state:
                transform_list = [s.strip() for s in state['transform'].split(',')]
                if len(screen_list) != len(transform_list):
                    raise ConsistencyError('The list of transform in {} doesn\'t have the same number of items than the `enabled` list.'.format(
                        context))
                for transform in transform_list:
                    _transform_check(transform, context)
            # check only_if_open
            if 'only_if_open' in state:
                only_if_open_list = [s.strip() for s in state['only_if_open'].split(',')]
                for s in only_if_open_list:
                    if s not in self.screens():
                        raise ConsistencyError('The screen `{}` referenced in {} is not defined.'.format(
                            s, context))
            
            # check focus
            if 'focus' in state:
                if state['focus'].strip() not in self.screens():
                    raise ConsistencyError('The screen `{}` referenced in {} is not defined.'.format(
                        state['focus'].strip(), context))
        
        # perform checks on the toggles
        for toggle_id in self.toggles():
            toggle = self.get_toggle(toggle_id)
            context = '[Toggle {}]'.format(toggle_id)
            # check the states list
            if not 'states' in toggle:
                raise ConsistencyError('You must define the `states` list in {}.'.format(
                    context))
            state_list = [s.strip() for s in toggle['states'].split(',')]
            for state_id in state_list:
                if state_id not in self.states():
                    raise ConsistencyError('The state `{}` referenced in {} is not defined.'.format(
                        state_id, context))


class ScreenManager:
    """Main screen manager program"""
    
    def __init__(self):
        # initialize a SwayOutput objectf
        self.so = SwayOutput()
    
    def main(self):
        """Start the screen manager"""
        # read the configuration file
        try:
            self.config = ScreenConfig()
        except configparser.DuplicateSectionError as dse:
            print(dse, file=sys.stderr)
            sys.exit(10)
        except configparser.ParsingError as pe:
            print(pe, file=sys.stderr)
            sys.exit(11)
        # check the configuration file
        try:
            self.config.check()
        except ConsistencyError as ce:
            print(ce, file=sys.stderr)
            sys.exit(12)
        
        # initalize the current state
        self.current_state = State(from_sway=self.so.screens)
        # load screen configurations
        self.config_states = {}
        for state_id in self.config.states():
            self.config_states[state_id] = State(from_config={
                'state_id': state_id,
                'config': self.config
            })
        # load toggles
        self.toggles = {}
        for toggle_id in self.config.toggles():
            states_str = self.config.get_toggle(toggle_id)['states']
            states_id = [s.strip() for s in states_str.split(',')]
            self.toggles[toggle_id] = states_id
        
        # parse command line
        parser = argparse.ArgumentParser(
            description='Sway screen configuration manager')
        parser.add_argument('--apply_state', metavar='STATE', help="Apply STATE")
        parser.add_argument('--toggle', help="Switch to the next state of TOGGLE")
        parser.add_argument('--show_states', help="show only the available state's list",
                    action="store_true")
        parser.add_argument('--show_toggles', help="show only the available toggle's list",
                    action="store_true")
        parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")
        args = parser.parse_args()
        
        if args.apply_state:
            self.apply_state(args.apply_state, args.verbose)
        elif args.toggle:
            self.toggle_state(args.toggle, args.verbose)
        else:
            self.list_options(args.verbose, args.show_states, args.show_toggles)
        
    def list_options(self, verbose=False, show_states=False, show_toggles=False):
        """List possible states and toggle"""
        state_possible = []
        state_enabled = []
        for state_id, state in self.config_states.items():
            if state.is_possible(self.current_state):
                state_possible.append(state_id)
            if state == self.current_state :
                 state_enabled.append(state_id)
        
        if show_states:
            for state_id in state_possible:
                print(state_id)
        
        if show_toggles:
            for toggle_id in self.toggles:
                print(toggle_id)
        
        if show_states or show_toggles:
            return
        
        print("Known states:")
        for state_id, state in self.config_states.items():
            print("    {}{}{}".format(
                state_id if not verbose else state,
                ' (available)' if state_id in state_possible else '',
                ' (current)' if state_id in state_enabled else ''
            ))
        print("Known toggles:")
        for toggle_id in self.toggles:
            print("    {} {}".format(toggle_id, 
                '[' + ', '.join(
                    (st + 
                        (' (cur)' if st in state_enabled else '') +
                        (' (!)' if not st in state_possible else '')
                    )
                    for st in self.toggles[toggle_id]
                ) + ']'
            ))
        if verbose:
            print("Current state:")
            print(self.current_state)
            
    def apply_state(self, state_id, verbose=False):
        """Apply state `state_id`"""
        try:
            wanted_state = self.config_states[state_id]
        except KeyError:
            print("The state {} doesn't exist.".format(state_id), file=sys.stderr)
            sys.exit(1)
        
        if not wanted_state.is_possible(self.current_state):
            print("The state {} can't be applied.".format(state_id), file=sys.stderr)
            sys.exit(2)
        
        # update wanted_state
        wanted_state.update(self.current_state)
        
        if verbose:
            print("Current state:")
            print(self.current_state)
            print("Wanted updated state:")
            print(wanted_state)
        
        # apply the state
        enabled_screens = [s for s in wanted_state.screens if s.enabled]
        disabled_screens = [s for s in wanted_state.screens if not s.enabled]
        for screen in enabled_screens:
            self.so.enable(screen.name[0])
            mode = None
            if screen.current_mode:
                mode = screen.current_mode
            elif screen.default_mode:
                mode = screen.default_mode
            if mode:
                self.so.set_mode(screen.name[0], '{width}x{height}'.format(**mode))
            if screen.position:
                x, y = screen.position['x'], screen.position['y']
                self.so.set_pos(screen.name[0], x, y)
            transform = None
            if screen.current_transform:
                transform = screen.current_transform
            elif screen.default_transform:
                transform = screen.default_transform
            if transform:
                self.so.rotate(screen.name[0], transform)
            if screen.scale:
                self.so.scale(screen.name[0], screen.scale)
            if screen.scale_filter:
                self.so.scale_filter(screen.name[0], screen.scale_filter)
            if screen.subpixel:
                self.so.subpixel(screen.name[0], screen.subpixel)
        for screen in disabled_screens:
            self.so.disable(screen.name[0])
        
        ###  move workspaces
        if wanted_state.workspaces:
            # list enabled screens
            enabled_scr_dict = {}
            for scr in enabled_screens:
                enabled_scr_dict[scr.screen_id] = scr
            # list existing workspaces
            existing_ws_list = self.so.workspaces.keys()
            # list existing workspaces to be moved
            all_but_managed_ws = []
            for scr_id, ws_list in wanted_state.workspaces:
                all_but_managed_ws += [
                    ws for ws in ws_list 
                    if ws != '*' and ws in existing_ws_list
                ]
        
            # for each screen:
            for scr_id, ws_list in wanted_state.workspaces:
                all_workspaces = False
                if scr_id in enabled_scr_dict.keys():
                    scr_name = enabled_scr_dict[scr_id].name[0]
                    # move standard workspaces to this screen
                    for workspace in ws_list:
                        if workspace == '*':
                            all_workspaces = True
                        elif workspace in existing_ws_list:
                            self.so.move_workspace(scr_name, workspace)
                    # if '*', move all workspaces except managed ones
                    if all_workspaces:
                        for workspace in existing_ws_list:
                            self.so.move_workspace(scr_name, workspace)
        
        # focus the screen
        if wanted_state.focus and wanted_state.focus in enabled_scr_dict.keys():
            self.so.focus(enabled_scr_dict[wanted_state.focus].name[0])
        
    
    def toggle_state(self, toggle_id, verbose=False):
        """Switch to the next state of `toggle_id`"""
        try:
            states_list = self.toggles[toggle_id]
        except KeyError:
            print("The toggle {} doesn't exist.".format(toggle_id), file=sys.stderr)
            sys.exit(3)
        
        # build the list of possible states
        possible_states = []
        for state_id in states_list:
            try:
                state = self.config_states[state_id]
            except KeyError:
                print("The state {} in the toggle {} doesn't exist.".format(
                    state_id, toggle_id))
                sys.exit(4)
            if state.is_possible(self.current_state):
                possible_states.append(state)
        
        # find current state
        current_state_idx = None
        for i, state in enumerate(possible_states):
            if state == self.current_state:
                current_state_idx = i
        
        # switch to the next state
        if current_state_idx is None or (current_state_idx + 1) >= len(possible_states):
            new_idx = 0
        else:
            new_idx = current_state_idx + 1
        
        new_state = possible_states[new_idx]
        self.apply_state(new_state.name, verbose)

if __name__ == '__main__':
    ScreenManager().main()
