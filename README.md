# Sway screen manager

The Sway screen manager is an utility program to help you to
manage your screens when using [Sway](https://swaywm.org/).

## How to configure it

You must create a configuration file in `~/.config/swayscreens.ini`.
You should look at `samples/swayscreens.ini` for a sample configuraton file.

Don't hesitate to use `swaymsg -t get_outputs` to retreive informations about
your currently connected screens.

There is three kind of sections in `swayscreens.ini`:

### [Screen ScreenID]

In this kind of section, you can describe a screen, either using its
connection `name` (like `eDP-1` or `HDMI-1`) or the `vendor_name` 
(like `Ancor Communications Inc ASUS VW193D 9ALMTF006407`).

You can also specify a `default_mode` (resolution) and a `default_transform`
(rotation) for this screen.

*Experimental* : you can also specify `scale`, `scale_filter` and `subpixel`
in a Screen section, following the documentation of `man 5 sway-output`.

For a laptop, you can define a `use_close_status` /proc path, to use the LID
state in states.

### [State StateID]

In this kind of section, you can describe which screens must be `enabled`. It's a
comma-separated list of ScreenID.
You can also specify a comma-separated list of `modes`, `positions` and `transform`.
Those list must be in the same order than the `enabled` list.

Then you can also define a screen must be use only if it is opened (for laptops),
put workspaces on a defined screen, and chose where will be the focus.

### [Toggle ToggleID]

In this kind of section, you can define a list of comma-separated `states`. The Toggle
will allow you to switch between all the states from this list.

## How to use it

To see which configurate `states` and `toggles` you have:

$ ./sway-screenmanager.py

To apply a given `state`:

$ ./sway-screenmanager.py --apply_state StateID

To go to the next state of a given `toggle`:

$ ./sway-screenmanager.py --toggle ToggleID

# How to contribute?

Please report your bugs and proposals in framagit: 
https://framagit.org/florencebiree/sway-screenmanager
